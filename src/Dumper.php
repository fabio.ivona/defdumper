<?php
	/**
	 * Created by PhpStorm.
	 * User: zioba
	 * Date: 14/12/2018
	 * Time: 11:09
	 */
	
	namespace DefStudio\Dumper;
	
	class Dumper {
		
		public static function dump($object, $return = false){
			
			
			$document = new \DOMDocument();
			
			
			$wrapper = $document->createElement('div');
			$wrapper->setAttribute('class', 'def_dump');
			$wrapper->setAttribute("style", "
                position:relative;
                border: 1px solid gray;
                background-color: #1f2229;
                color: #e5e5e5;
                font-size: 12px;
                font-family: monospace;
                padding: 5px;
                max-width: 1000px;
                -webkit-user-select: none;
                -moz-user-select: none;
                -ms-user-select: none;");
			$document->appendChild($wrapper);
			
			$wrapper->appendChild(self::recursive_dump($object, $document));
			
			
			if(!$return) {
				echo $document->saveHTML();
			}
			return $document->saveHTML();
		}
		
		
		private static function recursive_dump($object, \DOMDocument $document, $label = null, $depth = 0){
			
			
			if(is_numeric($object)) {
				if(empty($label)) {
					return self::number_node($object, $document);
				} else {
					$array_label = $document->createElement('div');
					$array_label->setAttribute('class', "array_label");
					$array_label->setAttribute("style", "margin-left: 6px;");
					
					
					$label_node = self::label_node($label, $document);
					$array_label->appendChild($label_node);
					
					$content_node = self::number_node($object, $document);
					$array_label->appendChild($content_node);
					
					return $array_label;
				}
			} else if(is_string($object)) {
				
				$array = json_decode($object, true);
				
				if($array != null && is_array($array)) {
					if(count($array)===1){
						reset($array);
						$first_key = key($array);
						return self::recursive_dump($array[$first_key], $document, $first_key, $depth);
					}else{
						
						return self::recursive_dump($array, $document, $label, $depth);
					}
					
				}
				
				
				if(empty($label)) {
					
					$text_container = $document->createElement("span");
					$text_container->appendChild(self::text_compressed_node($object, $document));
					$text_container->appendChild(self::text_expanded_node($object, $document));
					return $text_container;
					
				} else {
					$array_label = $document->createElement('div');
					$array_label->setAttribute('class', "array_label");
					$array_label->setAttribute("style", "margin-left: 6px;");
					
					
					$label_node = self::label_node($label, $document);
					$array_label->appendChild($label_node);
					
					
					$text_container = $document->createElement("span");
					$text_container->appendChild(self::text_compressed_node($object, $document));
					$text_container->appendChild(self::text_expanded_node($object, $document));
					
					$array_label->appendChild($text_container);
					
					return $array_label;
				}
			} else if(is_array($object)) {
				
				
				$array = $document->createElement('div');
				$array->setAttribute('class', 'array');
				
				if(empty($label) && $label!=0) {
					$label = 'array';
				}
				
				
				$array_label = $document->createElement('div');
				$array_label->setAttribute('class', "array_label");
				$array_label->setAttribute("style", "margin-left: 6px;");
				$array->appendChild($array_label);
				
				$label_node = self::label_node($label, $document);
				$array_label->appendChild($label_node);
				
				if(count($object) > 0) {
					
					if(count($object) === 1 && !is_numeric(key($object)) && is_array($object[key($object)])) {
						$expand_closed_node = self::expand_closed_node(key($object) . "[+]", $document, $depth);
						$array_label->appendChild($expand_closed_node);
						
						$expand_opened_start_node = self::opened_node_start(key($object) . "[", $document, $depth);
						$array_label->appendChild($expand_opened_start_node);
						
						$label = key($object);
						$object = $object[$label];
						
						
					} else {
						$expand_closed_node = self::expand_closed_node("array[" . count($object) . "]", $document, $depth);
						$array_label->appendChild($expand_closed_node);
						
						$expand_opened_start_node = self::opened_node_start("array[", $document, $depth);
						$array_label->appendChild($expand_opened_start_node);
					}
					
					
					$array_content_node = $document->createElement('div');
					
					if($depth > 0) {
						$array_content_node->setAttribute('class', 'array_content');
						$array_content_node->setAttribute("style", "border-left:1px solid green;margin-left:9px;display:none");
					} else {
						$array_content_node->setAttribute('class', 'array_content');
						$array_content_node->setAttribute("style", "border-left:1px solid green;margin-left:9px;display:block");
					}
					$array->appendChild($array_content_node);
					
					foreach($object as $content_label => $content) {
						$array_content_node->appendChild(self::recursive_dump($content, $document, $content_label, $depth + 1));
					}
					
					$expand_opened_end_node = self::opened_node_end($document, $depth);
					$array->appendChild($expand_opened_end_node);
				} else {
					$content_node = self::empty_array_node($document);
					$array_label->appendChild($content_node);
				}
				
				
				return $array;
			} else if(is_object($object)) {
				$array = self::convert_to_array($object);
				$class = get_class($object);
				var_dump($array);
				return self::recursive_dump($array, $document, $class, $depth);
			} else if(is_bool($object)) {
				if(empty($label)) {
					return self::boolean_node($object, $document);
				} else {
					$array_label = $document->createElement('div');
					$array_label->setAttribute('class', "array_label");
					$array_label->setAttribute("style", "margin-left: 6px;");
					
					
					$label_node = self::label_node($label, $document);
					$array_label->appendChild($label_node);
					
					$content_node = self::boolean_node($object, $document);
					$array_label->appendChild($content_node);
					
					return $array_label;
				}
			} else if($object === null) {
				if(empty($label)) {
					return self::null_node($document);
				} else {
					$array_label = $document->createElement('div');
					$array_label->setAttribute('class', "array_label");
					$array_label->setAttribute("style", "margin-left: 6px;");
					
					
					$label_node = self::label_node($label, $document);
					$array_label->appendChild($label_node);
					
					$content_node = self::null_node($document);
					$array_label->appendChild($content_node);
					
					return $array_label;
				}
			} else {
				if(empty($label)) {
					return self::text_node("DefDumper does not support this data type yet", $document);
				} else {
					$array_label = $document->createElement('div');
					$array_label->setAttribute('class', "array_label");
					$array_label->setAttribute("style", "margin-left: 6px;");
					
					
					$label_node = self::label_node($label, $document);
					$array_label->appendChild($label_node);
					
					$content_node = self::text_node("DefDumper does not support this data type yet", $document);
					$array_label->appendChild($content_node);
					
					return $array_label;
				}
			}
			
			
		}
		
		
		private static function text_node($content, \DOMDocument $document){
			$span = $document->createElement('span');
			$span->setAttribute('class', 'text');
			$span->setAttribute("style", "color: greenyellow;");
			
			$text = $document->createTextNode($content);
			$span->appendChild($text);
			return $span;
		}
		
		private static function text_compressed_node($content, \DOMDocument $document){
			$content = strlen($content) > 80 ? substr($content,0,80)."[..]" : $content;
			$span = $document->createElement('span');
			$span->setAttribute('class', 'text text_compressed');
			$span->setAttribute("style", "cursor: pointer;overflow: hidden;display:inline");
			$span->setAttribute('onClick', 'if(this.style.display==="inline"){this.style.display="none";}else{this.style.display="inline";}if(this.nextSibling.style.display==="inline"){this.nextSibling.style.display="none";}else{this.nextSibling.style.display="inline";}');
			$text = $document->createTextNode($content);
			$span->appendChild($text);
			
			
			return $span;
		}
		
		private static function text_expanded_node($content, \DOMDocument $document){
			$span = $document->createElement('span');
			$span->setAttribute('class', 'text text_expanded');
			$span->setAttribute("style", "cursor:pointer;display:none;");
			$span->setAttribute('onClick', 'if(this.style.display==="inline"){this.style.display="none";}else{this.style.display="inline";}if(this.previousSibling.style.display==="inline"){this.previousSibling.style.display="none";}else{this.previousSibling.style.display="inline";}');
			$text = $document->createTextNode($content);
			$span->appendChild($text);
			return $span;
		}
		
		private static function number_node($content, \DOMDocument $document){
			$span = $document->createElement('span');
			$span->setAttribute('class', 'number');
			$span->setAttribute("style", "color: deepskyblue;");
			
			$text = $document->createTextNode($content);
			$span->appendChild($text);
			return $span;
		}
		
		private static function boolean_node($content, \DOMDocument $document){
			if($content === true) {
				$span = $document->createElement('span');
				$span->setAttribute('class', 'boolean boolean_true');
				$span->setAttribute("style", "color: darkgreen;");
				
				$text = $document->createTextNode("true");
				$span->appendChild($text);
				return $span;
			} else {
				$span = $document->createElement('span');
				$span->setAttribute('class', 'boolean boolean_false');
				$span->setAttribute("style", "color: red;");
				
				$text = $document->createTextNode("false");
				$span->appendChild($text);
				return $span;
			}
			
		}
		
		private static function null_node(\DOMDocument $document){
			$span = $document->createElement('span');
			$span->setAttribute('class', 'null');
			$span->setAttribute("style", "color: gray;font-weight: bold;");
			
			$text = $document->createTextNode("null");
			$span->appendChild($text);
			return $span;
		}
		
		private static function empty_array_node(\DOMDocument $document){
			$span = $document->createElement('span');
			$span->setAttribute('class', 'empty');
			$span->setAttribute("style", "color:orange");
			
			$text = $document->createTextNode("array[0]");
			$span->appendChild($text);
			return $span;
		}
		
		private static function label_node($content, \DOMDocument $document){
			$span = $document->createElement('span', $content . " => ");
			$span->setAttribute('class', 'label');
			$span->setAttribute("style", "color: rgb(86, 219, 58);");
			return $span;
		}
		
		private static function expand_closed_node($label, \DOMDocument $document, $depth){
			
			$span = $document->createElement('span', $label);
			$span->setAttribute("onmouseover", "this.style.color='red';");
			$span->setAttribute("onmouseout", "this.style.color='orange';");
			if($depth > 0) {
				$span->setAttribute('class', 'expand expand-closed');
				$span->setAttribute('style', 'color:orange;cursor:pointer;display:inline');
			} else {
				$span->setAttribute('class', 'expand expand-closed');
				$span->setAttribute('style', 'cursor:pointer;display:none;color:orange;');
			}
			$span->setAttribute('onClick', 'if(this.style.display==="inline"){this.style.display="none";}else{this.style.display="inline";}if(this.nextSibling.style.display==="inline"){this.nextSibling.style.display="none";}else{this.nextSibling.style.display="inline";}if(this.parentNode.nextSibling.style.display==="block"){this.parentNode.nextSibling.style.display="none";}else{this.parentNode.nextSibling.style.display="block";}if(this.parentNode.nextSibling.nextSibling.style.display==="inline"){this.parentNode.nextSibling.nextSibling.style.display="none";}else{this.parentNode.nextSibling.nextSibling.style.display="inline";}');
			
			
			return $span;
		}
		
		private static function opened_node_start($label, \DOMDocument $document, $depth){
			$span = $document->createElement('span', $label);
			if($depth > 0) {
				$span->setAttribute('class', 'expand expand-opened-start');
				$span->setAttribute("style", "cursor:pointer;display:none;color:orange;");
			} else {
				$span->setAttribute('class', 'expand expand-opened-start');
				$span->setAttribute("style", "cursor:pointer;display:inline;color:orange;");
			}
			$span->setAttribute('onClick', "if(this.style.display==='inline'){this.style.display='none';}else{this.style.display='inline';}if(this.previousSibling.style.display==='inline'){this.previousSibling.style.display='none';}else{this.previousSibling.style.display='inline';}if(this.parentNode.nextSibling.style.display==='block'){this.parentNode.nextSibling.style.display='none';}else{this.parentNode.nextSibling.style.display='block';}if(this.parentNode.nextSibling.nextSibling.style.display==='inline'){this.parentNode.nextSibling.nextSibling.style.display='none';}else{this.parentNode.nextSibling.nextSibling.style.display='inline';}");
			
			return $span;
		}
		
		private static function opened_node_end(\DOMDocument $document, $depth){
			$span = $document->createElement('span', ']');
			if($depth > 0) {
				$span->setAttribute('class', "expand expand-opened-end");
				$span->setAttribute("style", "display:none;color:orange;margin-left:6px;");
			} else {
				$span->setAttribute('class', "expand expand-opened-end");
				$span->setAttribute("style", "color:orange;display:inline;margin-left:6px;");
			}
			return $span;
		}
		
		
		private static function convert_to_array($object){
			$result = self::object_to_array_recusive($object);
			return $result;
		}
		
		private static function object_to_array_recusive($object){
			$result = [];
			
			if(is_object($object)) {
				$array = (array)$object;
				foreach($array as $key => $value) {
					$key_split = explode("\0", $key);
					$key = end($key_split);
					$result[$key] = self::object_to_array_recusive($value);
				}
				return $result;
			} else if(is_array($object)) {
				$array = $object;
				foreach($array as $key => $value) {
					$result[$key] = self::object_to_array_recusive($value);
				}
				return $result;
			} else {
				return $object;
			}
			
			
		}
		
		
	}